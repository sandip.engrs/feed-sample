package com.sandip.sampleapp.app

import android.app.Application
import com.sandip.sampleapp.di.apiModule
import com.sandip.sampleapp.di.repositoryModule
import com.sandip.sampleapp.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class SampleApp : Application() {

    private val diModules = listOf(apiModule, repositoryModule, viewModelModule)

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@SampleApp)
            modules(diModules)
        }
    }
}