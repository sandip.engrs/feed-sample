package com.sandip.sampleapp.data.base

import java.io.IOException

sealed class RepositoryError(cause: Throwable, message: String? = null) :
    Throwable(message, cause) {

    class Network(cause: Throwable) : RepositoryError(cause, null)

    class Generic(cause: Throwable, message: String? = null) : RepositoryError(cause, message)

    companion object {
        fun fromException(e: Throwable, message: String? = null): RepositoryError {
            return when (e) {
                is IOException -> {
                    Network(e)
                }
                else -> Generic(e, message)
            }
        }
    }
}