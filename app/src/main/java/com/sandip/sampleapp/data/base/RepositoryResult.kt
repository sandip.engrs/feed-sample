package com.sandip.sampleapp.data.base

class RepositoryResult<T> private constructor() {

    var result: T? = null
        private set

    operator fun component1(): T? {
        return this.result
    }

    var error: RepositoryError? = null
        private set

    operator fun component2(): RepositoryError? {
        return this.error
    }

    companion object {

        fun <T> success(result: T) = RepositoryResult<T>().apply {
            this.result = result
        }

        fun <T> error(error: RepositoryError) = RepositoryResult<T>().apply {
            this.error = error
        }
    }
}

fun <T> RepositoryResult<T>.onSuccess(block: (T) -> Unit): RepositoryResult<T> {
    val (success, _) = this
    success?.let { block(it) }
    return this
}

suspend fun <T> RepositoryResult<T>.onSuccessBlocking(block: suspend (T) -> Unit): RepositoryResult<T> {
    val (success, _) = this
    success?.let { block(it) }
    return this
}

fun <T> RepositoryResult<T>.onError(block: (RepositoryError) -> Unit) {
    val (_, error) = this
    error?.let { block(it) }
}

fun <T, R> RepositoryResult<T>.flatMap(mapper: (T) -> R): RepositoryResult<R> {
    val (data, error) = this
    return data?.let {
        try {
            RepositoryResult.success(mapper(it))
        } catch (t: Throwable) {
            RepositoryResult.error(RepositoryError.fromException(t, "Something went wrong. Please try again"))
        }
    } ?: run { RepositoryResult.error(error!!) }
}