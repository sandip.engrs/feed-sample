package com.sandip.sampleapp.data.repository.feed

import com.google.firebase.firestore.FirebaseFirestore
import com.sandip.sampleapp.data.base.RepositoryError
import com.sandip.sampleapp.data.base.RepositoryResult
import com.sandip.sampleapp.data.repository.feed.model.Feed
import kotlinx.coroutines.tasks.await
import java.lang.Exception

class FeedRepository(
    private val firestore: FirebaseFirestore
): FeedRepositoryContract {
    override suspend fun getFeed(): RepositoryResult<List<Feed>> {
        return try {
            val result = firestore.collection(COLLECTION_NAME).get().await().toObjects(Feed::class.java)
            val success = RepositoryResult.success(result)
            success

        }catch (e: Exception){
            RepositoryResult.error(RepositoryError.fromException(e, e.localizedMessage))
        }
    }
}
private const val COLLECTION_NAME = "feed"