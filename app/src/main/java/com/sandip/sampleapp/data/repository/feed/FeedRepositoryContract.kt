package com.sandip.sampleapp.data.repository.feed

import com.sandip.sampleapp.data.base.RepositoryResult
import com.sandip.sampleapp.data.repository.feed.model.Feed

interface FeedRepositoryContract {
    suspend fun getFeed(): RepositoryResult<List<Feed>>
}