package com.sandip.sampleapp.data.repository.feed.model

data class Feed(
    val id: String = "",
    val img_url: String = "",
    val title: String = ""
)