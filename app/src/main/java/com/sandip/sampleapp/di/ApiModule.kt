package com.sandip.sampleapp.di

import com.google.firebase.firestore.FirebaseFirestore
import org.koin.dsl.module

val apiModule = module {

    single { provideFirebaseStore() }
}

private fun provideFirebaseStore(): FirebaseFirestore {
    return FirebaseFirestore.getInstance()
}