package com.sandip.sampleapp.di

import com.sandip.sampleapp.data.repository.feed.FeedRepository
import com.sandip.sampleapp.data.repository.feed.FeedRepositoryContract
import org.koin.dsl.bind
import org.koin.dsl.module

val repositoryModule = module {

    single<FeedRepositoryContract> {
        FeedRepository(
            get()
        )
    } bind FeedRepositoryContract::class
}