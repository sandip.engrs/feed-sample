package com.sandip.sampleapp.di

import com.sandip.sampleapp.ui.home.feed.FeedViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {

    viewModel { FeedViewModel(get()) }
}