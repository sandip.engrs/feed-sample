package com.sandip.sampleapp.ui.base

sealed class BaseEvent {
    class ShowError(val message: String): BaseEvent()
}