package com.sandip.sampleapp.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.sandip.sampleapp.BR
import com.sandip.sampleapp.base.BaseViewModel
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import kotlin.reflect.KClass

abstract class BaseSharedViewModelFragment<VM : BaseViewModel, VB : ViewDataBinding>(clazz: KClass<VM>) :
    BaseFragment() {

    protected lateinit var binding: VB
    abstract var layoutResId: Int
    protected val viewModel by sharedViewModel<VM>(clazz = clazz)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(layoutInflater, layoutResId, container, false)
        binding.setVariable(BR.viewModel, viewModel)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initSubscriptions()
    }

    open fun initSubscriptions() {}
}