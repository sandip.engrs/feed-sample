package com.sandip.sampleapp.ui.base

import android.os.Bundle
import android.os.PersistableBundle
import com.sandip.sampleapp.base.BaseViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import kotlin.reflect.KClass

abstract class BaseViewModelActivity<VM : BaseViewModel>(clazz: KClass<VM>) : BaseActivity() {

    protected val viewModel by viewModel<VM>(clazz = clazz)

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        initSubscriptions()
    }

    open fun initSubscriptions() {

    }
}