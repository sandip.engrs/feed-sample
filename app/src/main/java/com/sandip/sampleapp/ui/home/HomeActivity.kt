package com.sandip.sampleapp.ui.home

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.sandip.sampleapp.R

class HomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
    }
}