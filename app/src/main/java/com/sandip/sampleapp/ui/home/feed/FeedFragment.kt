package com.sandip.sampleapp.ui.home.feed

import android.widget.Toast
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DiffUtil
import com.sandip.sampleapp.R
import com.sandip.sampleapp.data.repository.feed.model.Feed
import com.sandip.sampleapp.databinding.FragmentFeedBinding
import com.sandip.sampleapp.ui.base.BaseSharedViewModelFragment
import com.sandip.sampleapp.ui.home.feed.model.FeedUIEvent
import com.sandip.sampleapp.ui.home.feed.model.FeedUIModel
import com.sandip.sampleapp.utils.DataBindingAdapterDiff
import com.sandip.sampleapp.utils.observeEvents
import com.sandip.sampleapp.utils.observeState

class FeedFragment :
    BaseSharedViewModelFragment<FeedViewModel, FragmentFeedBinding>(FeedViewModel::class) {

    override var layoutResId: Int = R.layout.fragment_feed

    private val feedListAdapter: FeedListAdapter by lazy { FeedListAdapter(viewModel) }

    override fun initViews() {
        viewModel.init()
        binding.feedRecyclerview.adapter = feedListAdapter
    }

    override fun initSubscriptions() {
        observeEvents(viewModel.uiEvent, ::handle)
        observeState(viewModel.uiModel, ::render)
    }

    private fun handle(uiEvent: FeedUIEvent) {
        when (uiEvent) {
            is FeedUIEvent.Progress -> toggleProgress(uiEvent.show)
            is FeedUIEvent.Error -> {
                toggleProgress(false)
                showError(uiEvent.message)
            }
        }
    }

    private fun showError(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    private fun toggleProgress(show: Boolean) {
        binding.progress.isVisible = show
    }

    private fun render(uiState: FeedUIModel) {
        when (uiState) {
            is FeedUIModel.Feeds -> {
                toggleProgress(false)
                feedListAdapter.submitList(uiState.feeds)
                binding.tvError.isVisible = false
            }
            is FeedUIModel.Empty -> {
                binding.tvError.isVisible = true
            }
        }
    }
}

private class FeedListAdapter(viewModel: FeedViewModel) :
    DataBindingAdapterDiff<Feed, FeedViewModel>(FeedDiffUtils(), viewModel) {
    override fun getItemViewType(position: Int): Int = R.layout.list_item_feed
}

private class FeedDiffUtils : DiffUtil.ItemCallback<Feed>() {
    override fun areItemsTheSame(oldItem: Feed, newItem: Feed): Boolean =
        oldItem == newItem

    override fun areContentsTheSame(oldItem: Feed, newItem: Feed): Boolean =
        oldItem.id == newItem.id
}

