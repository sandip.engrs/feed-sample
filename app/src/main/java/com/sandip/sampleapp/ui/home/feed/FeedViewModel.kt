package com.sandip.sampleapp.ui.home.feed

import android.util.Log
import androidx.lifecycle.viewModelScope
import com.sandip.sampleapp.base.BaseViewModel
import com.sandip.sampleapp.data.base.onError
import com.sandip.sampleapp.data.base.onSuccess
import com.sandip.sampleapp.data.repository.feed.FeedRepositoryContract
import com.sandip.sampleapp.ui.home.feed.model.FeedUIEvent
import com.sandip.sampleapp.ui.home.feed.model.FeedUIModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class FeedViewModel(private val repository: FeedRepositoryContract) :
    BaseViewModel() {

    private val _uiEvent = MutableStateFlow<FeedUIEvent>(FeedUIEvent.Init)
    private val _uiModel = MutableStateFlow<FeedUIModel>(FeedUIModel.Init)

    val uiEvent: StateFlow<FeedUIEvent> = _uiEvent
    val uiModel: StateFlow<FeedUIModel> = _uiModel

    fun init() = viewModelScope.launch {
        _uiEvent.value = FeedUIEvent.Progress(true)
        repository.getFeed().onSuccess {
            _uiModel.value = FeedUIModel.Feeds(it)

        }.onError {
            _uiEvent.value = FeedUIEvent.Error(it.localizedMessage)
        }
    }
}