package com.sandip.sampleapp.ui.home.feed.model

sealed class FeedUIEvent {
    object Init: FeedUIEvent()
    class Progress(val show: Boolean): FeedUIEvent()
    class Error(val message: String): FeedUIEvent()
}