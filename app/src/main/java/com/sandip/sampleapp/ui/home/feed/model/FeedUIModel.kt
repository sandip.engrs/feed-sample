package com.sandip.sampleapp.ui.home.feed.model

import com.sandip.sampleapp.data.repository.feed.model.Feed

sealed class FeedUIModel {
    object Init: FeedUIModel()
    class Feeds(val feeds: List<Feed>): FeedUIModel()
    object Empty: FeedUIModel()
}