package com.sandip.sampleapp.utils

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.annotation.LayoutRes
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.sandip.sampleapp.base.BaseViewModel
import com.sandip.sampleapp.BR

abstract class DataBindingAdapterDiff<T, VM : BaseViewModel>(
    diffCallback: DiffUtil.ItemCallback<T>,
    private val viewModel: VM
) : ListAdapter<T, DataBindingViewHolder<T, VM>>(diffCallback) {

    @LayoutRes
    abstract override fun getItemViewType(position: Int): Int

    override fun onCreateViewHolder(parent: ViewGroup, @LayoutRes layoutId: Int) =
        DataBindingViewHolder<T, VM>(parent.inflateBinding(layoutId))

    override fun onBindViewHolder(holder: DataBindingViewHolder<T, VM>, position: Int) =
        holder.bind(getItem(position), viewModel, position)
}

open class DataBindingViewHolder<T, VM : BaseViewModel>(open val binding: ViewDataBinding) :
    RecyclerView.ViewHolder(binding.root) {

    open fun bind(item: T, viewModel: VM, position: Int) {
        binding.setVariable(BR.item, item)
        binding.setVariable(BR.viewModel, viewModel)
        binding.executePendingBindings()
    }
}

inline fun <reified T : ViewDataBinding> ViewGroup.inflateBinding(
    @LayoutRes layoutId: Int,
    attachToParent: Boolean = false
): T {
    val layoutInflater = LayoutInflater.from(context)
    return DataBindingUtil.inflate(layoutInflater, layoutId, this, attachToParent)
}

@BindingAdapter("imageUrl")
fun ImageView.loadImage(url: String) {
    Glide.with(this.context).load(url).centerCrop().into(this)
}