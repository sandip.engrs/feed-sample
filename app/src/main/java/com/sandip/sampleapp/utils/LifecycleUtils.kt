package com.sandip.sampleapp.utils

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.sandip.sampleapp.base.observeOnLifecycle
import kotlinx.coroutines.flow.*

fun <T : Any, F : StateFlow<T>> AppCompatActivity.observeState(stateFlow: F, emit: (T?) -> Unit) {
    this.lifecycleScope.launchWhenStarted { stateFlow.collect { emit(it) } }
}

fun <T : Any, F : StateFlow<T>> Fragment.observeState(flow: F, emit: (T) -> Unit) {
    lifecycleScope.launchWhenStarted { flow.collect { emit(it) } }
}

inline fun <reified T : Any, F : Flow<T>> Fragment.observeEvents(
    flow: F,
    crossinline emit: (T) -> Unit
) { flow.observeOnLifecycle(viewLifecycleOwner) { emit(it) } }

inline fun <reified T : Any, F : Flow<T>> AppCompatActivity.observeEvents(
    flow: F,
    crossinline emit: (T) -> Unit
) { flow.observeOnLifecycle(this) { emit(it) } }